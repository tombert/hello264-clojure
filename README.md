# clojure-hello264

This is a relatively straightforward implementation of the Hello264 project, originally written in C, and ported to several other languages.  I figured I'd add to the mix and do something too. 

This is the very bare-minimum of h264; there's no compression, will probably yield bigger files than the input.  


## Usage

First you need to get your video file into a raw YUV format. 

```
ffmpeg -i input_file.mkv -s sqcif  outfile.yuv
```

After that, you can simply do this in the repo: 

```
lein run /path/to/input/file /path/to/outpath/file
```

After that, you should have a new file that you can feed back into ffmpeg to view it: 

```
ffmpeg -f h264 -i infile.264 -vcodec copy outfile.mkv
```

After that, you should be able to play this in pretty much any playere that supports H264. 


## Why? 

Obviously this basic implementation doesn't have much use in itself, but it serves as a good starting point into video processing and rendering.  The other implementations of this have all been more-or-less straight ports of the C version, but I felt that it would be useful to have something done in a more functional style. 

## TODOs? 

I'd like to get this working with GraalVM, but otherwise don't expect a ton of new features here.  

