(ns clojure-hello264.core
  (:import [java.nio.file Files Path Paths LinkOption StandardOpenOption OpenOption]
           [java.nio.file.attribute BasicFileAttributes FileAttribute PosixFileAttributes]
           [java.io InputStream])
  (:gen-class))

(def LUMA_WIDTH 128)
(def LUMA_HEIGHT 96)
(def CHROMA_WIDTH (/ LUMA_WIDTH 2))
(def CHROMA_HEIGHT (/ LUMA_HEIGHT 2))

(def SPS (byte-array [(short 0x00) (short 0x00) (short 0x00) (short 0x01) (short 0x67) (short 0x42) (short 0x00) (short 0x0a) (short 0xf8) (short 0x41) (short 0xa2)]))
(def PPS (byte-array [(short 0x00) (short 0x00) (short 0x00) (short 0x01) (short 0x68) (short 0xce) (short 0x38) (short 0x80)]))
(def SLICE_HEADER (byte-array [ (short 0x00) (short 0x00) (short 0x00) (short 0x01) (short 0x05) (short 0x88) (short 0x84) (short 0x21) (short 0xa0)]))
(def MACROBLOCK_HEADER (byte-array [(short 0x0d) (short 0x00)]))

(defrecord Frame [y cb cr])

(defn macroblock [i j frame]
;  (when (not= i j 0) (Files/write file MACROBLOCK_HEADER (into-array OpenOption [StandardOpenOption/APPEND])))

  (let [yval (transduce (comp 
                          (mapcat 
                            (fn [x]
                              (map 
                                (fn [y] 
                                  (get-in frame [:y x y])) (range (* 16 j) (* (+ 1 j) 16)))
                                 ))) conj (range (* 16 i) (* (+ 1 i) 16)))
        cbval (transduce (comp 
                          (mapcat 
                            (fn [x]
                              (map (fn [y] (get-in frame [:cb x y])) (range (* 8 j) (* (+ 1 j) 8)))
                                 ))) conj (range (* 8 i) (* (+ 1 i) 8)))
        crval (transduce (comp 
                          (mapcat 
                            (fn [x]
                              (map (fn [y] (get-in frame [:cr x y])) (range (* 8 j) (* (+ 1 j) 8)))
                                 ))) conj (range (* 8 i) (* (+ 1 i) 8)))
        final (concat yval cbval crval)
        ] 
    {:data final :header (if (not= i j 0) MACROBLOCK_HEADER nil)}))
    ;(Files/write file (byte-array final) (into-array OpenOption [StandardOpenOption/APPEND]))))

    ;(Files/write file (byte-array cbval) (into-array OpenOption [StandardOpenOption/APPEND]))
    ;(Files/write file (byte-array crval) (into-array OpenOption [StandardOpenOption/APPEND]))))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (let [empty-string-params (into-array String [])
        link-option-params (into-array LinkOption [])
        [^String input-filename ^String output-filename] args
        ^Path input-path (Paths/get input-filename empty-string-params)
        ^Path output-path (Paths/get output-filename empty-string-params)]
    (println "Processing!" input-filename )
    (Files/deleteIfExists output-path)
    (Files/createFile output-path (into-array FileAttribute []))
    (Files/write output-path SPS (into-array OpenOption [StandardOpenOption/APPEND]))
    (Files/write output-path PPS (into-array OpenOption [StandardOpenOption/APPEND]))
    (with-open 
      [^InputStream in-stream (Files/newInputStream input-path (into-array OpenOption []))]
      (loop [ ^InputStream infile in-stream]
        (let [yval (->> 
                     (range 0 96)
                     (transduce 
                       (comp 
                         (map 
                           (fn [_i]
                             (let [temp (->> (range 0 128) (map (fn [i] (short i))))
                                   buffer (byte-array temp)]
                               (.read infile buffer) 
                               buffer)    
                             ))) conj))
              cbval (->> 
                      (range 0 CHROMA_HEIGHT)
                      (transduce 
                        (comp 
                          (map 
                            (fn [_i]
                              (let [temp (->> (range 0 64) (map (fn [i] (short i))))
                                    buffer (byte-array temp)]
                                (.read infile buffer)
                                buffer))) 
                          ) conj))
              crval (->> (range 0 CHROMA_HEIGHT)
                         (transduce 
                           (comp 
                             (map 
                               (fn [_i] 
                                 (let [temp (->> (range 0 64) (map (fn [i] (short i))))
                                       buffer (byte-array temp) ]
                                   (.read infile buffer)
                                   buffer)
                                 ))) conj))
              frame (Frame. yval cbval crval)]
          
      (Files/write output-path SLICE_HEADER (into-array OpenOption [StandardOpenOption/APPEND]))
      (->> (range 0 (/ LUMA_HEIGHT 16)) 
           (transduce 
             (comp 
               (mapcat 
                 (fn [i]
                   (->> (range 0 (/ LUMA_WIDTH 16))
                        (transduce 
                          (comp
                            (map 
                              (fn [j]
                                (macroblock i j frame)))
                            ) conj)) ))
               (map 
                 (fn [final] 
                   (when (not= (:header final) nil)  
                     (Files/write output-path (:header final) 
                                  (into-array OpenOption [StandardOpenOption/APPEND])))
                   (Files/write output-path (byte-array (:data final)) (into-array OpenOption [StandardOpenOption/APPEND])))))
             (fn [& _] nil)))
      
      (Files/write output-path (byte-array [(short 0x80)]) (into-array OpenOption [StandardOpenOption/APPEND]))
      (when (>= (.available infile) 5)
        (recur infile)))))))
